#!/bin/bash
# Sun Feb 25 04:26:26 PST 2018
# haqler


# ERROR STATUS
NOT_SUDO_ERR=1
INSTALL_ERR=2
PARAM_ERR=3


# GLOBAL
PKGS="apache2 apache2-utils libcgi-fast-perl libapache2-mod-fcgid munin munin-node munin-plugins-extra"
#APACHE24_CFG="/etc/munin/apache24.conf"
#MUNIN_CFG="/etc/munin/munin.conf"
MUNIN_CONFIG_DIR="/etc/munin"
USER_CONFIG_DIR=`pwd`


check_user_input()
{
	if [ "$UID" -ne 0 ]; then
		echo "To run '`basename "$0"`' you need sudo privilegios." >&2
		echo "Try: 'sudo .../`basename "$0"`'" >&2
		exit $NOT_SUDO_ERR
	fi

	while [ "$#" -gt 0 ]
	do
		case "$1" in
			'-m')
					USER_MAIL="$2";
					shift 2
					;;

			'-d')
					USER_CONFIG_DIR="$2";
					shift 2
					;;

			'-h')
					HOST_NAME="$2";
					shift 2
					;;

			*)
					echo "Unkown parameter: $1" >&2
					echo "Usage: `basename $0` [-m your_mail] [-d path_to_config_files] [-h host_name]" >&2
					exit $PARAM_ERR;
					;;
		esac

	done

}


install_pkg()
{
	check_pkgs="$PKGS"
	install_pkgs=''

	for pkg in $check_pkgs
	do
		#echo "pkg: $pkg"
		dpkg -l "$pkg" &>/dev/null
		if [ "$?" -eq 0 ]; then
			echo "Package '$pkg' already installed"

		else
			echo "Package '$pkg' will be installed"
			install_pkgs="$install_pkgs $pkg"	

		fi

	done

	if [ -n "$install_pkgs" ]; then
		echo "apt-get install $install_pkgs"
		apt-get install $install_pkgs
		
		if [ "$?" -ne 0 ]; then
			exit $INSTALL_ERR
		fi

	fi

}


configure_munin()
{
	if [ ! -e "${MUNIN_CONFIG_DIR}/apache24.conf" ]; then
		echo "File ${MUNIN_CONFIG_DIR}/apache24.conf does not exist!" >&2
		exit $INSTALL_ERR
	fi

	if [ ! -e "${MUNIN_CONFIG_DIR}/munin.conf" ]; then
		echo "File ${MUNIN_CONFIG_DIR}/munin.conf does not exit!" >&2
		exit $INSTALL_ERR
	fi


	if [ ! -e "${USER_CONFIG_DIR}/apache24.conf" ]; then
		echo "File ${USER_CONFIG_DIR}/apache24.conf does not exist!" >&2
		exit $PARAM_ERR
	fi

	if [ ! -e "${USER_CONFIG_DIR}/munin.conf" ]; then
		echo "File ${USER_CONFIG_DIR}/munin.conf does not exit!" >&2
		exit $PARAM_ERR
	fi

	if [ -z "$USER_MAIL" ]; then
		read -p "Enter your mail: " USER_MAIL
	
	fi


#	create backup of config files
	cp "${MUNIN_CONFIG_DIR}/apache24.conf"  "${MUNIN_CONFIG_DIR}/apache24.conf.bak_`date +%d%m%Y`"
	cp "${MUNIN_CONFIG_DIR}/munin.conf"  "${MUNIN_CONFIG_DIR}/munin.conf.bak_`date +%d%m%Y`"

#	copy user config files to munin config dir
	cp "${USER_CONFIG_DIR}/apache24.conf"  "${MUNIN_CONFIG_DIR}/apache24.conf"

	if [ -n "$HOST_NAME" ]; then
		sed -e "s/user_mail/${USER_MAIL}/" -e "s/localhost.localdomain/${HOST_NAME}/"  \
									"${USER_CONFIG_DIR}/munin.conf"  >"${MUNIN_CONFIG_DIR}/munin.conf"

	else
		sed -e "s/user_mail/${USER_MAIL}/" "${USER_CONFIG_DIR}/munin.conf"  >"${MUNIN_CONFIG_DIR}/munin.conf"

	fi

}


run_munin()
{
	# enable fcgid module in apache
	a2enmod fcgid

	# restart apache2
	service apache2 restart

	# restart munin
	service munin-node restart

}

	

# call functions
echo "*****Checking user input*****"
check_user_input $*
echo -e "Done\n"

echo "*****Installation*****"
install_pkg
echo -e "Done\n"

echo "*****Configuration*****"
configure_munin
echo -e "Done\n"

echo "*****Sturtup*****"
run_munin
echo -e "Done\n"
echo "Finish"


exit 0

